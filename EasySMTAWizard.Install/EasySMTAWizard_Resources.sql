BEGIN TRANSACTION
if not exists(select * from app_resource where sys_lang_id = 1 and [app_name] = 'GRINGlobalClientCuratorTool' and form_name = 'EasySMTAWizard')  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasySMTAWizard','Text', 'Easy SMTA Wizard v{0}', 48,getdate(),48,getdate())
else
   Print 'GRINGlobalClientCuratorTool/EasySMTAWizard language English already exist. Verify/Update your information.'

if not exists(select * from app_resource where sys_lang_id = 1 and [app_name] = 'GRINGlobalClientCuratorTool' and form_name = 'EasySMTAConfigForm')  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasySMTAConfigForm','Text', 'SMTA Configuration', 48,getdate(),48,getdate())
else
   Print 'GRINGlobalClientCuratorTool/EasySMTAConfigForm, language English already exist. Verify/Update your information.'

if not exists(select * from app_resource where sys_lang_id = 2 and [app_name] = 'GRINGlobalClientCuratorTool' and form_name = 'EasySMTAConfigForm')  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasySMTAWizard','Text', 'Asistente de SMTA v{0}', 48,getdate(),48,getdate())
else
   Print 'GRINGlobalClientCuratorTool/EasySMTAWizard language Spanish already exist. Verify/Update your information.'

if not exists(select * from app_resource where sys_lang_id = 2 and [app_name] = 'GRINGlobalClientCuratorTool' and form_name = 'EasySMTAConfigForm')  
  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasySMTAConfigForm','Text', 'Configuración SMTA', 48,getdate(),48,getdate())
else
   Print 'GRINGlobalClientCuratorTool/EasySMTAConfigForm, language Spanish already exist. Verify/Update your information.'
COMMIT TRANSACTION


---SMTA Settings (Optional). If you want to configurate the Provider information into app_seting table.

BEGIN TRANSACTION
--PID of your institution. By example, CIMMYT's PID is: 00AO84 --> Replace PID_Number by : 00AO84
if not exists(select * from app_setting where category_tag = 'SMTA' and name = 'SMTA-PID-Provider')   
   INSERT INTO app_setting (category_tag,name,value,created_by,created_date,owned_by,owned_date)
   VALUES('SMTA','SMTA-PID-Provider','00AO84',48,getdate(),48,getdate())
else
   Print 'SMTA-PID-Provider already exist. Verify/Update your information.'

--Provider information. The owner of the accessions (owned_by field) not always is the "official" provider of the material so, you can add and set a cooperator how as an official provider.
--In this case, Provider_Cooperator_id = Cooperator_id that have defined. 
if not exists(select * from app_setting where category_tag = 'SMTA' and name = 'SMTA-Provider-Cooperador-id')   
   INSERT INTO app_setting (category_tag,name,value,created_by,created_date,owned_by,owned_date)
   VALUES('SMTA','SMTA-Provider-Cooperador-id','153663',48,getdate(),48,getdate())
else
   Print 'SMTA-Provider-Cooperador-id already exist. Verify/Update your information.'

--Document location. Where is the SMTA document archived as follows p=with the Provider, s=with the Secretariat, o=elsewhere
--In this case, always the document is with the provider. 
if not exists(select * from app_setting where category_tag = 'SMTA' and name = 'SMTA-docLoc')   
   INSERT INTO app_setting (category_tag,name,value,created_by,created_date,owned_by,owned_date)
   VALUES('SMTA','SMTA-docLoc','p',48,getdate(),48,getdate())
else
   Print 'SMTA-docLoc already exist. Verify/Update your information.'

--Retrival information. How the SMTA document can be retrieved
--You can set your own value.
if not exists(select * from app_setting where category_tag = 'SMTA' and name = 'SMTA-retInfo')   
   INSERT INTO app_setting (category_tag,name,value,created_by,created_date,owned_by,owned_date)
   VALUES('SMTA','SMTA-retInfo','On demand from the Head of CIMMYT Intellectual Property Office',48,getdate(),48,getdate())
else
   Print 'SMTA-retInfo already exist. Verify/Update your information.'
COMMIT TRANSACTION


