﻿using EasySMTAWizard.Model;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace EasySMTAWizard
{
    /// <summary>
    /// SMTA Controller helps orchestrate the interaction between the curator tool 
    /// and the SMTA server 
    /// </summary>
    public class SMTAController : ISMTAController
    {
        private SharedUtils _sharedUtils;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sharedUtils">Curator Tool SharedUtils</param>
        public SMTAController(SharedUtils sharedUtils)
        {
            this._sharedUtils = sharedUtils;
            this.Configuration = new Configuration(string.Format("{0}\\GRIN-Global\\Curator Tool\\smta_{1}.txt", System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _sharedUtils.Username));
        }

        /// <summary>
        /// Launch configuration dialog
        /// </summary>
        public void Configure()
        {
            EasySMTAConfigForm cf = new EasySMTAConfigForm(this);
            cf.StartPosition = FormStartPosition.CenterParent;
            cf.ShowDialog();
        }

        /// <summary>
        /// Send the SMTA request to the SMTA server
        /// </summary>
        /// <param name="request">SMTA request</param>
        /// <returns>SMTA response object</returns>
        public SMTAResponse Send(SMTARequest request)
        {
            // Serialize the Request Object to XML
            byte[] body = null;
            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    XmlSerializer requestSerializer = new XmlSerializer(typeof(SMTARequest));
                    requestSerializer.Serialize(writer, request, request.Namespaces);
                    body = stream.ToArray();
                }
            }

            // Strip "Byte Order Mark" from array so '<' is the first character
            int gtIndex = -1;
            for (int i = 0; i < body.Length && gtIndex < 0; i++)
            {
                if (body[i] == 60) // Check for '<' character
                {
                    gtIndex = i;
                }
            }
            if (gtIndex > 0)
                body = body.Skip(gtIndex).ToArray();

            // Compress if too many annex records. Per SMTA documentation, requests may fail
            // if a document is sent with more than 100 annex records
            bool compressed = false;
            if (request.Materials != null && request.Materials.Count >= 100)
            {
                body = Compress(body);
                compressed = true;
            }

            // Post the message to SMTA over HTTP
            HttpClient httpClient = new HttpClient();
            MultipartFormDataContent content = new MultipartFormDataContent
            {
                { new StringContent(this.Configuration.Username), "username" },
                { new StringContent(this.Configuration.Password), "password" },
                { new StringContent(compressed ? "y" : "n"), "compressed" },
                { new ByteArrayContent(body), "xml" }
            };
            UriBuilder ub = new UriBuilder(this.Configuration.ServerUri + "/index.php?r=extsys/uploadxml");
            HttpResponseMessage response = httpClient.PostAsync(ub.Uri, content).Result;

            // Deserialize the SMTA response message
            string cb = response.Content.ReadAsStringAsync().Result;
            if (!response.IsSuccessStatusCode || (cb.Length > 0 && cb[0] != '<'))
            {
                // HTTP send failed, so throw an exception
                throw new SMTAException(cb);
            }

            // Deserialize the Response
            XmlSerializer responseSerializer = new XmlSerializer(typeof(SMTAResponse));
            SMTAResponse resp = (SMTAResponse)responseSerializer.Deserialize(new StringReader(cb));

            return resp;
        }

        /// <summary>
        /// GRINGlobal Client Shared Utilities
        /// </summary>
        public SharedUtils SharedUtils
        {
            get
            {
                return _sharedUtils;
            }
        }

        /// <summary>
        /// SMTA configuration
        /// </summary>
        public Configuration Configuration { get; private set; }

        /// <summary>
        /// Compress the byte stream using GZip
        /// </summary>
        /// <param name="data">Uncompressed data</param>
        /// <returns>Compressed data</returns>
        private byte[] Compress(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data must be non-null");

            using (var compressIntoMs = new MemoryStream())
            {
                using (var gzs = new BufferedStream(new GZipStream(compressIntoMs,
                CompressionMode.Compress), 64 * 1024)) // 64k buffer
                {
                    gzs.Write(data, 0, data.Length);
                }
                return compressIntoMs.ToArray();
            }
        }
    }
}
