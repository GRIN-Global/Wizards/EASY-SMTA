﻿using EasySMTAWizard.Model;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySMTAWizard
{
    /// <summary>
    /// SMTA Controller Interface
    /// </summary>
    public interface ISMTAController
    {
        /// <summary>
        /// SMTA Connection Configuration
        /// </summary>
        Configuration Configuration { get; }

        /// <summary>
        /// Launch configuration dialog
        /// </summary>
        void Configure();

        /// <summary>
        /// Send a request to SMTA
        /// </summary>
        /// <param name="request">SMTA request</param>
        /// <returns></returns>
        SMTAResponse Send(SMTARequest request);

        /// <summary>
        /// Reference to Curator Tool Utils class
        /// </summary>
        SharedUtils SharedUtils { get; }
    }
}
