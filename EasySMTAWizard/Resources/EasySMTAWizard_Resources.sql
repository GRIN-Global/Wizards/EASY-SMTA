BEGIN TRANSACTION

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasySMTAWizard','Text', 'Easy SMTA Wizard v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (1,'GRINGlobalClientCuratorTool','EasySMTAConfigForm','Text', 'SMTA Configuration', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasySMTAWizard','Text', 'Asistente de SMTA v{0}', 48,getdate(),48,getdate())

  INSERT INTO app_resource (sys_lang_id,app_name,form_name,app_resource_name, display_member,created_by,created_date,owned_by,owned_date) 
  VALUES (2,'GRINGlobalClientCuratorTool','EasySMTAConfigForm','Text', 'Configuración SMTA', 48,getdate(),48,getdate())
  
COMMIT TRANSACTION