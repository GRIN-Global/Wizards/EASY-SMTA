﻿namespace EasySMTAWizard
{
    partial class EasySMTAWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EasySMTAWizard));
            this.btnConfigure = new System.Windows.Forms.Button();
            this.dgOrders = new System.Windows.Forms.DataGridView();
            this.order_request_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.smta_reported = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.smta_reported_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.symbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uploadstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.language = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provtype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provaddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provcountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provemail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rectype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recpid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recaddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reccountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.retinfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.ssStatusStrip = new System.Windows.Forms.StatusStrip();
            this.lblRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblErrorCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUploading = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbStatusDetails = new System.Windows.Forms.GroupBox();
            this.rtStatusDetails = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
            this.ssStatusStrip.SuspendLayout();
            this.gbStatusDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConfigure
            // 
            this.btnConfigure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConfigure.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnConfigure.Location = new System.Drawing.Point(12, 297);
            this.btnConfigure.Name = "btnConfigure";
            this.btnConfigure.Size = new System.Drawing.Size(75, 23);
            this.btnConfigure.TabIndex = 400;
            this.btnConfigure.Text = "&Configure...";
            this.btnConfigure.UseVisualStyleBackColor = true;
            this.btnConfigure.Click += new System.EventHandler(this.btnConfigure_Click);
            // 
            // dgOrders
            // 
            this.dgOrders.AllowUserToAddRows = false;
            this.dgOrders.AllowUserToDeleteRows = false;
            this.dgOrders.AllowUserToResizeRows = false;
            this.dgOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.order_request_id,
            this.smta_reported,
            this.smta_reported_date,
            this.date,
            this.symbol,
            this.recname,
            this.uploadstatus,
            this.type,
            this.language,
            this.provtype,
            this.provname,
            this.pid,
            this.provaddress,
            this.provcountry,
            this.provemail,
            this.rectype,
            this.recpid,
            this.recaddress,
            this.reccountry,
            this.shipname,
            this.location,
            this.retinfo,
            this.pdf});
            this.dgOrders.Location = new System.Drawing.Point(12, 12);
            this.dgOrders.MultiSelect = false;
            this.dgOrders.Name = "dgOrders";
            this.dgOrders.ReadOnly = true;
            this.dgOrders.RowHeadersVisible = false;
            this.dgOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOrders.ShowCellErrors = false;
            this.dgOrders.ShowCellToolTips = false;
            this.dgOrders.ShowEditingIcon = false;
            this.dgOrders.ShowRowErrors = false;
            this.dgOrders.Size = new System.Drawing.Size(563, 279);
            this.dgOrders.TabIndex = 300;
            this.dgOrders.SelectionChanged += new System.EventHandler(this.dgOrders_SelectionChanged);
            // 
            // order_request_id
            // 
            this.order_request_id.DataPropertyName = "order_request_id";
            this.order_request_id.HeaderText = "Order Request ID";
            this.order_request_id.Name = "order_request_id";
            this.order_request_id.ReadOnly = true;
            this.order_request_id.Width = 85;
            // 
            // smta_reported
            // 
            this.smta_reported.DataPropertyName = "smta_reported";
            this.smta_reported.HeaderText = "smta_reported";
            this.smta_reported.Name = "smta_reported";
            this.smta_reported.ReadOnly = true;
            this.smta_reported.Visible = false;
            // 
            // smta_reported_date
            // 
            this.smta_reported_date.DataPropertyName = "smta_reported_date";
            this.smta_reported_date.HeaderText = "smta_reported_date";
            this.smta_reported_date.Name = "smta_reported_date";
            this.smta_reported_date.ReadOnly = true;
            this.smta_reported_date.Visible = false;
            // 
            // date
            // 
            this.date.DataPropertyName = "date";
            this.date.HeaderText = "Date";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            this.date.Width = 90;
            // 
            // symbol
            // 
            this.symbol.DataPropertyName = "symbol";
            this.symbol.HeaderText = "Symbol";
            this.symbol.Name = "symbol";
            this.symbol.ReadOnly = true;
            this.symbol.Width = 70;
            // 
            // recname
            // 
            this.recname.DataPropertyName = "recname";
            this.recname.HeaderText = "Recipient";
            this.recname.Name = "recname";
            this.recname.ReadOnly = true;
            this.recname.Width = 130;
            // 
            // uploadstatus
            // 
            this.uploadstatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.uploadstatus.DataPropertyName = "uploadstatus";
            this.uploadstatus.HeaderText = "Status";
            this.uploadstatus.Name = "uploadstatus";
            this.uploadstatus.ReadOnly = true;
            // 
            // type
            // 
            this.type.DataPropertyName = "type";
            this.type.HeaderText = "type";
            this.type.Name = "type";
            this.type.ReadOnly = true;
            this.type.Visible = false;
            // 
            // language
            // 
            this.language.DataPropertyName = "language";
            this.language.HeaderText = "language";
            this.language.Name = "language";
            this.language.ReadOnly = true;
            this.language.Visible = false;
            // 
            // provtype
            // 
            this.provtype.DataPropertyName = "provtype";
            this.provtype.HeaderText = "provtype";
            this.provtype.Name = "provtype";
            this.provtype.ReadOnly = true;
            this.provtype.Visible = false;
            // 
            // provname
            // 
            this.provname.DataPropertyName = "provname";
            this.provname.HeaderText = "provname";
            this.provname.Name = "provname";
            this.provname.ReadOnly = true;
            this.provname.Visible = false;
            // 
            // pid
            // 
            this.pid.DataPropertyName = "pid";
            this.pid.HeaderText = "pid";
            this.pid.Name = "pid";
            this.pid.ReadOnly = true;
            this.pid.Visible = false;
            // 
            // provaddress
            // 
            this.provaddress.DataPropertyName = "provaddress";
            this.provaddress.HeaderText = "provaddress";
            this.provaddress.Name = "provaddress";
            this.provaddress.ReadOnly = true;
            this.provaddress.Visible = false;
            // 
            // provcountry
            // 
            this.provcountry.DataPropertyName = "provcountry";
            this.provcountry.HeaderText = "provcountry";
            this.provcountry.Name = "provcountry";
            this.provcountry.ReadOnly = true;
            this.provcountry.Visible = false;
            // 
            // provemail
            // 
            this.provemail.DataPropertyName = "provemail";
            this.provemail.HeaderText = "provemail";
            this.provemail.Name = "provemail";
            this.provemail.ReadOnly = true;
            this.provemail.Visible = false;
            // 
            // rectype
            // 
            this.rectype.DataPropertyName = "rectype";
            this.rectype.HeaderText = "rectype";
            this.rectype.Name = "rectype";
            this.rectype.ReadOnly = true;
            this.rectype.Visible = false;
            // 
            // recpid
            // 
            this.recpid.DataPropertyName = "recpid";
            this.recpid.HeaderText = "recpid";
            this.recpid.Name = "recpid";
            this.recpid.ReadOnly = true;
            this.recpid.Visible = false;
            // 
            // recaddress
            // 
            this.recaddress.DataPropertyName = "recaddress";
            this.recaddress.HeaderText = "recaddress";
            this.recaddress.Name = "recaddress";
            this.recaddress.ReadOnly = true;
            this.recaddress.Visible = false;
            // 
            // reccountry
            // 
            this.reccountry.DataPropertyName = "reccountry";
            this.reccountry.HeaderText = "reccountry";
            this.reccountry.Name = "reccountry";
            this.reccountry.ReadOnly = true;
            this.reccountry.Visible = false;
            // 
            // shipname
            // 
            this.shipname.DataPropertyName = "shipname";
            this.shipname.HeaderText = "shipname";
            this.shipname.Name = "shipname";
            this.shipname.ReadOnly = true;
            this.shipname.Visible = false;
            // 
            // location
            // 
            this.location.DataPropertyName = "location";
            this.location.HeaderText = "location";
            this.location.Name = "location";
            this.location.ReadOnly = true;
            this.location.Visible = false;
            // 
            // retinfo
            // 
            this.retinfo.DataPropertyName = "retinfo";
            this.retinfo.HeaderText = "retinfo";
            this.retinfo.Name = "retinfo";
            this.retinfo.ReadOnly = true;
            this.retinfo.Visible = false;
            // 
            // pdf
            // 
            this.pdf.DataPropertyName = "pdf";
            this.pdf.HeaderText = "pdf";
            this.pdf.Name = "pdf";
            this.pdf.ReadOnly = true;
            this.pdf.Visible = false;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnTransfer.Location = new System.Drawing.Point(646, 297);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(75, 23);
            this.btnTransfer.TabIndex = 100;
            this.btnTransfer.Text = "&Upload";
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // ssStatusStrip
            // 
            this.ssStatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ssStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRecordCount,
            this.lblErrorCount,
            this.lblUploading,
            this.pbProgress});
            this.ssStatusStrip.Location = new System.Drawing.Point(0, 327);
            this.ssStatusStrip.Name = "ssStatusStrip";
            this.ssStatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.ssStatusStrip.Size = new System.Drawing.Size(814, 22);
            this.ssStatusStrip.TabIndex = 209;
            this.ssStatusStrip.Text = "statusStrip1";
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(61, 17);
            this.lblRecordCount.Text = "Records: 0";
            // 
            // lblErrorCount
            // 
            this.lblErrorCount.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblErrorCount.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblErrorCount.Name = "lblErrorCount";
            this.lblErrorCount.Size = new System.Drawing.Size(49, 17);
            this.lblErrorCount.Text = "Errors: 0";
            this.lblErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUploading
            // 
            this.lblUploading.AutoSize = false;
            this.lblUploading.Margin = new System.Windows.Forms.Padding(50, 3, 0, 2);
            this.lblUploading.Name = "lblUploading";
            this.lblUploading.Size = new System.Drawing.Size(118, 17);
            this.lblUploading.Text = "Uploading Data:";
            this.lblUploading.Visible = false;
            // 
            // pbProgress
            // 
            this.pbProgress.AutoSize = false;
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(135, 16);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.pbProgress.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClose.Location = new System.Drawing.Point(727, 297);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 110;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbStatusDetails
            // 
            this.gbStatusDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStatusDetails.Controls.Add(this.rtStatusDetails);
            this.gbStatusDetails.Location = new System.Drawing.Point(594, 12);
            this.gbStatusDetails.Name = "gbStatusDetails";
            this.gbStatusDetails.Size = new System.Drawing.Size(208, 279);
            this.gbStatusDetails.TabIndex = 211;
            this.gbStatusDetails.TabStop = false;
            this.gbStatusDetails.Text = "Status Details";
            // 
            // rtStatusDetails
            // 
            this.rtStatusDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtStatusDetails.BackColor = System.Drawing.SystemColors.Control;
            this.rtStatusDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtStatusDetails.BulletIndent = 1;
            this.rtStatusDetails.CausesValidation = false;
            this.rtStatusDetails.Location = new System.Drawing.Point(17, 66);
            this.rtStatusDetails.Name = "rtStatusDetails";
            this.rtStatusDetails.Size = new System.Drawing.Size(177, 193);
            this.rtStatusDetails.TabIndex = 1;
            this.rtStatusDetails.Text = "";
            // 
            // EasySMTAWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 349);
            this.Controls.Add(this.gbStatusDetails);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.ssStatusStrip);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.dgOrders);
            this.Controls.Add(this.btnConfigure);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(394, 235);
            this.Name = "EasySMTAWizard";
            this.Text = "EasySMTAWizard";
            this.Load += new System.EventHandler(this.EasySMTAWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
            this.ssStatusStrip.ResumeLayout(false);
            this.ssStatusStrip.PerformLayout();
            this.gbStatusDetails.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConfigure;
        private System.Windows.Forms.DataGridView dgOrders;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.StatusStrip ssStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel lblErrorCount;
        private System.Windows.Forms.ToolStripStatusLabel lblUploading;
        private System.Windows.Forms.ToolStripProgressBar pbProgress;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox gbStatusDetails;
        private System.Windows.Forms.RichTextBox rtStatusDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn order_request_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn smta_reported;
        private System.Windows.Forms.DataGridViewTextBoxColumn smta_reported_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn recname;
        private System.Windows.Forms.DataGridViewTextBoxColumn uploadstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn language;
        private System.Windows.Forms.DataGridViewTextBoxColumn provtype;
        private System.Windows.Forms.DataGridViewTextBoxColumn provname;
        private System.Windows.Forms.DataGridViewTextBoxColumn pid;
        private System.Windows.Forms.DataGridViewTextBoxColumn provaddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn provcountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn provemail;
        private System.Windows.Forms.DataGridViewTextBoxColumn rectype;
        private System.Windows.Forms.DataGridViewTextBoxColumn recpid;
        private System.Windows.Forms.DataGridViewTextBoxColumn recaddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn reccountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipname;
        private System.Windows.Forms.DataGridViewTextBoxColumn location;
        private System.Windows.Forms.DataGridViewTextBoxColumn retinfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn pdf;
    }
}