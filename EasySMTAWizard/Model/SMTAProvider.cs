﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// SMTA Provider represents the part either sending or receibing the material
    /// </summary>
    public class SMTAProvider
    {
        [XmlElement("type")]
        public string Type { get; set; }
        [XmlElement("pid")]
        public string Pid { get; set; }
        [XmlElement("name")]
        public string Name { get; set; }
        [XmlElement("surname")]
        public string Surname { get; set; }
        [XmlElement("address")]
        public string Address { get; set; }
        [XmlElement("country")]
        public string Country { get; set; }
        [XmlElement("email")]
        public string Email { get; set; }
    }
}
