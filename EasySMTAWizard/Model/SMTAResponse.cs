﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// SMTA Response object
    /// </summary>
    [XmlRoot("smta")]
    public class SMTAResponse
    {
        /// <summary>
        /// Symbol of the SMTA sent in the request
        /// </summary>
        [XmlElement("symbol")]
        public string Symbol { get; set; }

        /// <summary>
        /// Provider PID
        /// </summary>
        [XmlElement("providerPID")]
        public string ProviderPid { get; set; }

        /// <summary>
        /// Result of the tranfer. OK for success or KO for failure
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

        /// <summary>
        /// Array of errors
        /// </summary>
        [XmlElement("error")]
        public List<SMTAError> Error { get; set; }

        /// <summary>
        /// Indicates if successful
        /// </summary>
        public bool Success
        {
            get
            {
                return Result == "OK";
            }
        }
    }
}
