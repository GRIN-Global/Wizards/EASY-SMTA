﻿using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// SMTA error returned with response
    /// </summary>
    public class SMTAError
    {
        [XmlElement("code")]
        public string Code { get; set; }
        [XmlElement("msg")]
        public string Message { get; set; }
    }
}