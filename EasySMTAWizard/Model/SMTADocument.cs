﻿using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// SMTA Document is a component of the SMTA Request
    /// </summary>
    public class SMTADocument
    {
        [XmlElement("location")]
        public string Location { get; set; }
        [XmlElement("retInfo")]
        public string RetInfo { get; set; }
        [XmlElement("pdf")]
        public string Pdf { get; set; }
    }
}