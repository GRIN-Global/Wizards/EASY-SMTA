﻿using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// SMTA Material is a component of the SMTA Request
    /// </summary>
    [XmlType("material")]
    [XmlRoot("material")]
    public class SMTAMaterial
    {
        [XmlElement("crop")]
        public string Crop { get; set; }
        [XmlElement("sampleID")]
        public string SampleID { get; set; }
        [XmlElement("pud")]
        public string Pud { get; set; }
        [XmlElement("ancestry")]
        public string Ancestry { get; set; }
    }
}