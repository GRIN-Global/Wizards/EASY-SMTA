﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EasySMTAWizard.Model
{
    /// <summary>
    /// Main SMTA Request Object
    /// </summary>
    [XmlRoot("smta", IsNullable = false)]
    public class SMTARequest
    {
        
        private XmlSerializerNamespaces namespaces;

        /// <summary>
        /// Contructor
        /// </summary>
        public SMTARequest()
        {
            this.namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[]
            {
                new XmlQualifiedName(string.Empty, "urn:SMTA")
            });
            Materials = new List<SMTAMaterial>();
        }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Namespaces
        {
            get { return this.namespaces; }
        }

        [XmlElement("symbol")]
        public string Symbol { get; set; }
        [XmlIgnore]
        public DateTime? Date { get; set; }
        [XmlElement("type")]
        public string Type { get; set; }
        [XmlElement("language")]
        public string Language { get; set; }
        [XmlElement("provider")]
        public SMTAProvider Provider { get; set; }
        [XmlElement("recipient")]
        public SMTAProvider Recipient { get; set; }
        [XmlElement("shipName")]
        public string ShipName { get; set; }
        [XmlArray("annex1")]
        public List<SMTAMaterial> Materials { get; set; }
        [XmlElement("document")]
        public SMTADocument Document { get; set; }
        [XmlElement("date")]
        public string DateString {
            get
            {
                if (Date == null)
                    return null;
                else
                    return Date.Value.ToString("yyyy-MM-dd");
            }
            set { }
        }

        /// <summary>
        /// Get errors with the request data before sending
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetErrors()
        {
            List<string> errors = new List<string>();
            if (Materials == null || Materials.Count == 0)
                errors.Add(SMTAResources.NoOrderItems);

            return errors;
        }
    }
}
