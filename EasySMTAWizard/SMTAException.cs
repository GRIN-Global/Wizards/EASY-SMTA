﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySMTAWizard
{

    /// <summary>
    /// Enumeration of expected error types
    /// </summary>
    public enum SMTAErrorType : byte
    {
        Unknown = 0,
        Authentication,
        Configuration,
        BadData
    }

    /// <summary>
    /// SMTA Exception simplifying error responses to sending SMTA documents,
    /// and translating to an issue type enumeration
    /// </summary>
    public class SMTAException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Error message</param>
        public SMTAException(string message) : base(message)
        {
            switch (message)
            {
                case "Access denied":
                    this.IssueType = SMTAErrorType.Authentication;
                    break;
                case "Request sent over HTTP instead of HTTPS":
                    this.IssueType = SMTAErrorType.Configuration;
                    break;
                case "Empty XML document":
                    this.IssueType = SMTAErrorType.BadData;
                    break;
                default:
                    this.IssueType = SMTAErrorType.Unknown;
                    break;
            }
        }

        /// <summary>
        /// Issue type
        /// </summary>
        public SMTAErrorType IssueType { get; set; }
    }
}
