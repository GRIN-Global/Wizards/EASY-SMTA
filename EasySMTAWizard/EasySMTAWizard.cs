﻿using EasySMTAWizard.Model;
using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasySMTAWizard
{
    /// <summary>
    /// Interface required by the GRINGlobal Curator Tool wizard framework
    /// </summary>
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    /// <summary>
    /// Primary EasySMTA Wizard dialog
    /// </summary>
    public partial class EasySMTAWizard : Form, IGRINGlobalDataWizard
    {
        private string _originalPKeys;
        private string _orderRequestPKeys;
        private SMTAController _controller;
        private DataTable _orderRequests;
        private DataTable _orderRequestItems;
        private DataTable _dataErrors;
        private BackgroundWorker _bw = null;
        private Cursor _origCursor = null;

        #region IGRINGlobalDataWizard
        /// <summary>
        /// Name of the wizard displayed in the CuratorTool
        /// </summary>
        public string FormName
        {
            get
            {
                return SMTAResources.WizardName;
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                return new DataTable();
            }
        }

        /// <summary>
        /// Required key type
        /// </summary>
        public string PKeyName
        {
            get
            {
                return "order_request_id";
            }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pKeys">Keys of selected Order Request objects</param>
        /// <param name="sharedUtils">Curator Tool utilities</param>
        public EasySMTAWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            if (sharedUtils != null)
            {
                this._controller = new SMTAController(sharedUtils);

                // Create errors table
                this._dataErrors = new DataTable();
                this._dataErrors.Columns.AddRange(new[] {
                    new DataColumn("order_request_id", typeof(int)),
                    new DataColumn("message")
                    });
            }

            _originalPKeys = pKeys;
            // Ignore all pkey tokens except the orderrequest_id pkeys...
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":ORDERREQUESTID") _orderRequestPKeys = pkeyToken;
            }

            // Configure background worker
            _bw = new BackgroundWorker();
            _bw.WorkerReportsProgress = true;
            _bw.RunWorkerCompleted += UploadRunWorkerCompleted;
            _bw.ProgressChanged += UploadProgressChanged;
            _bw.DoWork += UploadSMTAData;
        }

        /// <summary>
        /// Form load even
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EasySMTAWizard_Load(object sender, EventArgs e)
        {
            this.Text = FormName;

            // Verify that the wizard has the required setup to function
            if (!this.VerifySetup())
            {
                MessageBox.Show(SMTAResources.InstallationErrorMsg, SMTAResources.InstallationError, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }

            pbProgress.Style = ProgressBarStyle.Marquee;
            this.btnTransfer.Enabled = _controller.Configuration.IsConfigured;

            // Update Controls based on Languages
            if (this.components != null && this.components.Components != null) _controller.SharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _controller.SharedUtils.UpdateControls(this.Controls, this.Name);

            // Load data for passed
            LoadData(_orderRequestPKeys);
        }

        /// <summary>
        /// Launch the configuration dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfigure_Click(object sender, EventArgs e)
        {
            this._controller.Configure();
            this.btnTransfer.Enabled = _controller.Configuration.IsConfigured;
        }

        /// <summary>
        /// Close the wizard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Initiate the transfer of SMTA records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTransfer_Click(object sender, EventArgs e)
        {
            // Protect against running twice at once
            if (_bw.IsBusy)
                return;

            // Prepare the UI
            btnTransfer.Enabled = false;
            btnConfigure.Enabled = false;
            SetStatusDetails();
            dgOrders.ClearSelection();
            lblUploading.Visible = true;
            pbProgress.Visible = true;
            lblRecordCount.Text = string.Format(SMTAResources.Records, 0, this._orderRequests.DefaultView.Count);
            lblErrorCount.Text = string.Format(SMTAResources.Errors, 0);
            lblErrorCount.ForeColor = SystemColors.ControlText;

            // Change cursor to the wait cursor...
            _origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Start the background worker
            _bw.RunWorkerAsync();
        }

        /// <summary>
        /// Primary worker function to upload data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadSMTAData(object sender, DoWorkEventArgs e)
        {
            int totalRecords = _orderRequests.Rows.Count;
            int recordsProcessed = 0;
            int errorCount = 0;

            // Clear Prior Errors
            this._dataErrors.Clear();
            foreach (DataRow rw in this._orderRequests.Rows)
                rw["uploadstatus"] = "";

            try
            {
                // Walk through each order request
                foreach (DataRow request in _orderRequests.Rows)
                {
                    int requestId = (int)request["order_request_id"];

                    // Create Data Transfer Object
                    SMTARequest smtaRequest = null;
                    List<string> dataErrors = new List<string>();
                    try
                    {
                        smtaRequest = CreateSMTADocument(requestId);
                        dataErrors.AddRange(smtaRequest.GetErrors());
                    }
                    catch (Exception ex)
                    {
                        Exception currentEx = ex;
                        while (currentEx.InnerException != null)
                            currentEx = currentEx.InnerException;
                        dataErrors.Add(string.Format(SMTAResources.UnexpectedError, currentEx.Message));
                    }

                    // Get any errors with object data
                    if (dataErrors.Count() == 0)
                    {
                        // Transfer the Object
                        var smtaResponse = _controller.Send(smtaRequest);

                        // If not successful,add returned error message to the errors list
                        if (!smtaResponse.Success)
                        {
                            dataErrors.AddRange(smtaResponse.Error.Select(err => err.Message).ToArray());
                        }
                    }

                    // Update status column for record
                    request["uploadstatus"] = (dataErrors.Count() == 0) ? SMTAResources.UploadCompleteNoErrors : string.Format(SMTAResources.UploadCompleteErrors, dataErrors.Count());

                    if (dataErrors.Count() > 0)
                    {
                        dataErrors.ForEach(de => _dataErrors.Rows.Add(new object[] { requestId, de }));
                        ++errorCount;
                    } else
                    {
                        // No errors. Store Response in OBJECT REQUEST_ACTION
                        if (!RecordSMTATransfer(requestId)) ++errorCount;
                    }

                    // Update progress in UI
                    _bw.ReportProgress(100 * ++recordsProcessed / totalRecords, new UploadStatus() { Processed = recordsProcessed, Errors = errorCount, Total = totalRecords });
                }
            }
            catch (SMTAException se)
            {
                // If an SMTAException occurs, we attempt to provide an informative response to the user
                switch (se.IssueType)
                {
                    case SMTAErrorType.Authentication:
                        MessageBox.Show(SMTAResources.BadCredentialsError, SMTAResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case SMTAErrorType.Configuration:
                        MessageBox.Show(SMTAResources.IncorrectServerError, SMTAResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case SMTAErrorType.BadData:
                    case SMTAErrorType.Unknown:
                    default:
                        MessageBox.Show(SMTAResources.UnexpectedError, SMTAResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
                e.Result = null;
                return;
            }
            catch (Exception ex)
            {
                Exception currentEx = ex;
                while (currentEx.InnerException != null)
                    currentEx = currentEx.InnerException;
                MessageBox.Show(string.Format(SMTAResources.UnexpectedError, currentEx.Message), SMTAResources.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Result = null;
                return;
            }

            // Return error count for the end of run event
            e.Result = errorCount;
        }

        /// <summary>
        /// Update the status details UI text based on results of transfer for the selected row
        /// </summary>
        /// <param name="status">Status message</param>
        /// <param name="errorDetails">List of errors</param>
        private void SetStatusDetails(string status = null, IEnumerable<string> errorDetails = null)
        {
            bool hasDetails = errorDetails != null && errorDetails.Count() > 0;

            rtStatusDetails.Clear();
            rtStatusDetails.SelectionAlignment = hasDetails ? HorizontalAlignment.Left : HorizontalAlignment.Center;
            rtStatusDetails.SelectedText = (string.IsNullOrWhiteSpace(status) ? SMTAResources.SelectRowForStatus : status) + "\n\n";

            if (hasDetails)
            {
                bool first = true;
                rtStatusDetails.SelectionIndent = 12;
                rtStatusDetails.BulletIndent = 6;
                rtStatusDetails.SelectionBullet = true;
                foreach (var ed in errorDetails)
                {
                    rtStatusDetails.SelectedText = first ? ed : "\n" + ed;
                    first = false;
                }
            }
        }

        /// <summary>
        /// Update the UI with progress as the records are sent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is UploadStatus)
            {
                var status = e.UserState as UploadStatus;
                lblRecordCount.Text = string.Format(SMTAResources.Records, status.Processed, status.Total);
                lblErrorCount.Text = string.Format(SMTAResources.Errors, status.Errors);
                if (status.Errors > 0)
                    lblErrorCount.ForeColor = Color.Red;
            }
            Cursor.Current = Cursors.WaitCursor;
        }

        /// <summary>
        /// Reset the UI once the transfer is complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnTransfer.Enabled = true;
            btnConfigure.Enabled = true;
            lblUploading.Visible = false;
            pbProgress.Visible = false;

            // Restore cursor
            Cursor.Current = _origCursor;

            if (e.Result == null)
                return;

            // Report Errors to User
            int errors = 0;
            if (e.Result is int)
                errors = (int)e.Result;

            if (errors == 0)
            {
                MessageBox.Show(SMTAResources.UploadCompleteNoErrors, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(string.Format(SMTAResources.UploadCompleteErrors, errors), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Load the order request records to the UI table
        /// </summary>
        /// <param name="parameters"></param>
        private void LoadData(string parameters = "")
        {

            DataSet ds;
            // Get the accession table and bind it to the main form on the General tabpage...
            ds = _controller.SharedUtils.GetWebServiceData("get_SMTA_Report", parameters, 0, 0);

            if (ds.Tables.Contains("get_SMTA_Report"))
            {
                _orderRequests = ds.Tables["get_SMTA_Report"].Copy();
                if(!_orderRequests.Columns.Contains("uploadstatus"))
                {
                    _orderRequests.Columns.Add("uploadstatus");
                }

                ds = _controller.SharedUtils.GetWebServiceData("get_SMTA_Report_Material", parameters, 0, 0);
                if (ds.Tables.Contains("get_SMTA_Report_Material"))
                {
                    _orderRequestItems = ds.Tables["get_SMTA_Report_Material"].Copy();
                }
            }

            dgOrders.DataSource = _orderRequests;

            lblRecordCount.Text = string.Format(SMTAResources.Records, 0, _orderRequests.DefaultView.Count);
            lblErrorCount.Text = string.Format(SMTAResources.Errors, 0);
        }

        /// <summary>
        /// Create an SMTARequest object from a GRINGlobal order request record
        /// </summary>
        /// <param name="orderRequestId">Order Request Id</param>
        /// <returns>SMTA Request object</returns>
        private SMTARequest CreateSMTADocument(int orderRequestId)
        {
            // Filter the order request and order request item records.
            var orderRow = _orderRequests.Select(string.Format("order_request_id={0}", orderRequestId)).FirstOrDefault();
            if (orderRow == null)
                return null;
            var orderItemRows = _orderRequestItems.Select(string.Format("order_request_id={0}", orderRequestId));

            // Create SMTA Request object and populate with GRIN Global data
            var request = new SMTARequest();
            request.Symbol = orderRow["symbol"].ToString();
            if(!string.IsNullOrWhiteSpace(orderRow["date"].ToString()))
                request.Date = DateTime.Parse(orderRow["date"].ToString());
            request.Type = orderRow["type"].ToString();
            request.Language = orderRow["language"].ToString();
            request.ShipName = orderRow["shipname"].ToString();

            // Provider
            request.Provider = new SMTAProvider();
            request.Provider.Type = orderRow["provtype"].ToString();
            request.Provider.Name = orderRow["provname"].ToString();
            request.Provider.Pid = orderRow["pid"].ToString();
            request.Provider.Address = orderRow["provaddress"].ToString();
            request.Provider.Country = orderRow["provcountry"].ToString();
            request.Provider.Email = orderRow["provemail"].ToString();

            // Recipient
            request.Recipient = new SMTAProvider();
            request.Recipient.Type = orderRow["rectype"].ToString();
            request.Recipient.Pid = orderRow["recpid"].ToString();
            request.Recipient.Name = orderRow["recname"].ToString();
            request.Recipient.Address = orderRow["recaddress"].ToString();
            request.Recipient.Country = orderRow["reccountry"].ToString();

            // Document
            request.Document = new SMTADocument();
            request.Document.Location = orderRow["location"].ToString();
            request.Document.RetInfo = orderRow["retinfo"].ToString();
            string path = orderRow["pdf"].ToString();
            if (!string.IsNullOrWhiteSpace(path) && path.EndsWith(".pdf"))
            {
                // If a document is attached to the order, upload with the SMTA
                var pdfData = this._controller.SharedUtils.GetAttachment(path);
                if (pdfData.Length > 0)
                    request.Document.Pdf = Convert.ToBase64String(pdfData);
            }

            // Add Order Request items
            foreach (var item in orderItemRows)
            {
                var material = new SMTAMaterial();
                material.SampleID = item["sampleid"].ToString();
                material.Crop = item["crop"].ToString();
                material.Pud = item["pud"].ToString();
                material.Ancestry = item["ancestry"].ToString();
                request.Materials.Add(material);
            }

            return request;
        }

        /// <summary>
        /// Update Order Request actions to record the transfer of the SMTA
        /// </summary>
        /// <param name="orderRequestId">Order Request Id</param>
        /// <returns>Success</returns>
        private bool RecordSMTATransfer(int orderRequestId)
        {
            bool success = false;
            var ds = _controller.SharedUtils.GetWebServiceData("get_order_request_action", "", 0, 0);
            if (ds.Tables.Contains("get_order_request_action"))
            {
                var row = ds.Tables["get_order_request_action"].NewRow();
                row["order_request_id"] = orderRequestId;
                row["action_name_code"] = "SMTAACCEPT";
                row["started_date"] = DateTime.Now;
                row["completed_date"] = DateTime.Now;
                ds.Tables["get_order_request_action"].Rows.Add(row);

                DataSet results = _controller.SharedUtils.SaveWebServiceData(ds.GetChanges());
                if (results.Tables[0].Rows.Count == 0)
                {
                    success = true;
                }
                else
                {
                    foreach (DataRow resultRow in results.Tables[0].Rows)
                    {
                        this._dataErrors.Rows.Add(new[] { orderRequestId, resultRow["Message"] });
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Ensure supporting dataviews are present
        /// </summary>
        private bool VerifySetup()
        {
            bool result = true;

            // SMTA Dataviews defined
            var dataviews = _controller.SharedUtils.GetWebServiceData("get_dataview_list", "", 0, 0);
            if (!dataviews.Tables.Contains("get_dataview_list") || dataviews.Tables["get_dataview_list"].Select("dataview_name like 'get_SMTA_Report%'").Count() < 2)
            {
                result = false;
            }

            // Provider PID Set
            if (string.IsNullOrWhiteSpace(_controller.SharedUtils.GetAppSettingValue("SMTA-PID-Provider")))
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Helper class providing a mechanism to pass progress info back to the
        /// UI thread.
        /// </summary>
        private class UploadStatus
        {
            /// <summary>
            /// Number of errors
            /// </summary>
            public int Errors { get; set; }

            /// <summary>
            /// Number of records processed
            /// </summary>
            public int Processed { get; set; }

            /// <summary>
            /// Total number of records to process
            /// </summary>
            public int Total { get; set; }
        }

        /// <summary>
        /// When a order request row is select, update the status details text 
        /// with any errrs that occurred 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgOrders_SelectionChanged(object sender, EventArgs e)
        {
            if (dgOrders.SelectedRows.Count == 0)
                SetStatusDetails();
            else
            {
                string rowStatus = dgOrders.SelectedRows[0].Cells["uploadstatus"].Value.ToString();
                if (string.IsNullOrWhiteSpace(rowStatus))
                    SetStatusDetails(SMTAResources.UploadDataForStatus);
                else
                {
                    var errors = _dataErrors.Select(string.Format("order_request_id = {0}", dgOrders.SelectedRows[0].Cells["order_request_id"].Value.ToString())).Select(er => er["message"].ToString());
                    SetStatusDetails(rowStatus, errors);
                }
            }
        }
    }
}
