## **Easy SMTA Wizard**

Easy-SMTA is the Information Technology System developed in support of the users of the Multilateral System of Access and Benefit-sharing of the International Treaty on Plant Genetic Resources for Food and Agriculture (ITPGRFA).
The EASY-SMTA wizard was develped to support the publishing of order request data to Easy-SMTA.

## How to install the SMTA Wizar for Curator Tool

1. Download the files: "EasySMTAWizard.dll", "Newtonsoft.Json.dll" and "stdole.dll" from [here](https://gitlab.com/GRIN-Global/Wizards/EASY-SMTA/-/tree/master/EasySMTAWizard.Install).
1. Unblock the DLLs files. Right click the DLL file, select properties and check "Unblock"
1. Copy the file "EasySMTAWizard.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards" folder.
1. Copy the files "Newtonsoft.Json.dll" and "stdole.dll" to "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool" folder.
1. Run Curator Tool.

## Importing the Easy SMTA datviews

Don't forget to import the dataviews **get_SMTA_Report** and **get_SMTA_Report_Material.dataviewxml** through GRIN-Global Admin-Tool, the Easy SMTA datviews are available [here](https://gitlab.com/GRIN-Global/Wizards/EASY-SMTA/-/tree/master/EasySMTAWizard.Install) 


The Easy SMTA wizard dataviews needs a configuration values to work, open the **EasySMTAWizard_Resources.sql** file in your SQL Server client tool and execute it.
